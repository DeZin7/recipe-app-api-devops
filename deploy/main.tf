terraform {
  backend "s3" {
    bucket         = "recipe-app-api-dezindevops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-west-1"
  version = "~> 2.54.0"
}

# variable "prefix" {
#   default = "raad"
# }

# variable "project" {
#   default = "recipe-app-api-devops"
# }

# variable "contact" {
#   default = "marcusandre77@icloud.com"
# }

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}

